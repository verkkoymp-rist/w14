import React, { useState, useEffect } from 'react';
import './App.css';

function App() {
  const [message, setMessage] = useState('');
  const [showHistory, setShowHistory] = useState(false);

  const initialChatHistory = [
    "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>",
    "<p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...</p>",
    "<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...</p>",
    "<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur...</p>",
    "<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum...</p>"
  ];

  const [chatHistory, setChatHistory] = useState(initialChatHistory);

  
  useEffect(() => {
    const storedChatHistory = JSON.parse(localStorage.getItem('chatHistory')) || initialChatHistory;
    setChatHistory(storedChatHistory);
  }, []);

  const handleSendMessage = () => {
    if (!message.trim()) return; 

    const updatedChatHistory = [...chatHistory, `<p>${message}</p>`];
    setChatHistory(updatedChatHistory);
    localStorage.setItem('chatHistory', JSON.stringify(updatedChatHistory));
    setMessage('');
  };

  const handleToggleMessages = () => {
    setShowHistory(!showHistory); 
  };

  return (
    <div className="chat-container">
      <div className="chat-input-container">
        <input
          type="text"
          className="chat-input"
          placeholder="Type a message..."
          value={message}
          onChange={(e) => setMessage(e.target.value)}
          onKeyPress={(e) => e.key === 'Enter' && handleSendMessage()}
        />
        <button onClick={handleSendMessage}>Send</button>
        <button onClick={handleToggleMessages}>
          {showHistory ? 'Hide messages' : 'Show messages'}
        </button>
      </div>

      {showHistory && (
        <div className="chat-history" dangerouslySetInnerHTML={{ __html: chatHistory.join(' ') }}>
        </div>
      )}
    </div>
  );
}

export default App;
